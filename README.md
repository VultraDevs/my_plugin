# `my_plugin`

An example plugin for [Veloren](https://www.veloren.net/) that implements `/ping` chat command.

You can view the tutorial for this plugin [here](https://book.veloren.net/contributors/modders/writing-a-plugin.html).

You can edit the tutorial by submitting merge requests to [this repository](https://gitlab.com/veloren/book/).
